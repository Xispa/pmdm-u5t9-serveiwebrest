package dam.androidignacio.u5t9httpclient;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.INotificationSideChannel;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.IntUnaryOperator;

public class U5T9HttpClientActivity extends AppCompatActivity implements View.OnClickListener, ListView.OnItemClickListener {

    // constants
    private final static String URL_GEONAMES = "http://api.geonames.org/wikipediaSearchJSON";
    private final static String USER_NAME = "xisp";
    private final static int ROWS = 10;

    private final static String URL_OPENWEATHER = "api.openweathermap.org/data/2.5/weather";
    private final static String OPEN_WEATHER_API_KEY = "67be11f25f0c02531d7c817af4acaf3c";

    // attributes
    private EditText etPlaceName;
    private Button btSearch;
    private ListView lvSearchResult;
    private ArrayList<GeonamesPlace> listSearchResult;
    private ExecutorService executor;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_u5_t9_http_client);

        setUI();
    }

    private void setUI() {
        etPlaceName = findViewById(R.id.etPlaceName);
        btSearch = findViewById(R.id.btSearch);
        btSearch.setOnClickListener(this);

        // create an empty ArrayLIst
        listSearchResult = new ArrayList<>();

        lvSearchResult = findViewById(R.id.lvSearchResult);

        // set adapter to listView
        lvSearchResult.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1,
                listSearchResult));
        lvSearchResult.setOnItemClickListener(this);

        progressBar = findViewById(R.id.progressBar);
    }

    @Override
    public void onClick(View view) {

        progressBar.setVisibility(View.VISIBLE);

        if (isNetworkAvailable()) {
            // check if user has written a place

            String place = etPlaceName.getText().toString();

            if (!place.isEmpty()) {
                URL geonamesURL;
                try {
                    // url = new URL(URL_GEONAMES + "?q=" + place + "&maxRows=" + ROWS + "&userName=" + USER_NAME);
                    // use better a builder to avoid a malformed query string
                    Uri.Builder builder = new Uri.Builder();
                    builder.scheme("http")
                            .authority("api.geonames.org")
                            .appendPath("wikipediaSearchJSON")
                            .appendQueryParameter("lang", "es")
                            .appendQueryParameter("q", place)
                            .appendQueryParameter("maxRows", String.valueOf(ROWS))
                            .appendQueryParameter("username", USER_NAME);

                    geonamesURL = new URL(builder.build().toString());

                    closeKeyboard();
                    startGeoNamesBackgroundTask(geonamesURL, progressBar);

                } catch (MalformedURLException e) {
                    Log.i("URL", e.getMessage());
                }
            } else {
                Toast.makeText(this, "Write a place to search", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, "Sorry, network is not available", Toast.LENGTH_LONG).show();
        }

    }

    @SuppressWarnings("deprecation")
    private Boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Network nw = connectivityManager.getActiveNetwork();
            if (nw == null) return false;

            NetworkCapabilities actNw = connectivityManager.getNetworkCapabilities(nw);

            return actNw != null && (actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ||
                    actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR));
        } else {
            NetworkInfo nwInfo = connectivityManager.getActiveNetworkInfo();
            return nwInfo != null && nwInfo.isConnected();
        }
    }

    private void startGeoNamesBackgroundTask(URL url, ProgressBar progressBar) {
        final int CONNECTION_TIMEOUT = 10000;
        final int READ_TIMEOUT = 7000;

        executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());

        executor.execute(new Runnable() {
            @Override
            public void run() {
                HttpURLConnection urlConnection = null;
                final ArrayList<GeonamesPlace> searchResult = new ArrayList<>();

                try {
                    // 1. Create connection object by invoking the openConnection method on a URL
                    urlConnection = (HttpURLConnection) url.openConnection();

                    // 2. Setup connection parameters
                    urlConnection.setConnectTimeout(CONNECTION_TIMEOUT);
                    urlConnection.setReadTimeout(READ_TIMEOUT);

                    // 3. connect to remote object (urlConnection)
                    urlConnection.connect();

                    // 4. The remote object becomes available.
                    //    The header fileds and the contents of the remote object can be accessed
                    getGeonamesData(urlConnection, searchResult);

                } catch (IOException e) {
                    Log.i("IOException", e.getMessage());
                    //searchResult.add("IOException: " + e.getMessage());
                } catch (JSONException e) {
                    Log.i("JSONException", e.getMessage());
                    //searchResult.add("JSONException: " + e.getMessage());
                } finally {
                    if (urlConnection != null) urlConnection.disconnect();
                }

                // Finally, update UI
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (searchResult.size() > 0) {
                            // set new results to list adapter
                            ArrayAdapter<GeonamesPlace> adapter = (ArrayAdapter<GeonamesPlace>) lvSearchResult.getAdapter();
                            adapter.clear();
                            for (GeonamesPlace geonamesPlace : searchResult) {
                                adapter.add(geonamesPlace);
                            }
                            adapter.notifyDataSetChanged();

                        } else { // use if possible ApplicationContext()
                            Toast.makeText(getApplicationContext(),
                                    "Not possible to contact " + URL_GEONAMES, Toast.LENGTH_LONG).show();
                        }

                    }
                });
            }
        });

        progressBar.setVisibility(View.GONE);

    }

    private void getGeonamesData(HttpURLConnection urlConnection, ArrayList<GeonamesPlace> searchResult)
            throws IOException, JSONException {

        // Check if response was OK (response=200)
        if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            // read data stream from url
            String resultStream = readStream(urlConnection.getInputStream());

            // create JSON object from result stream
            JSONObject json = new JSONObject(resultStream);
            JSONArray jArray = json.getJSONArray("geonames");


            if (jArray.length() > 0) {
                // fill list with data form summary attribute
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject item = jArray.getJSONObject(i);
                    searchResult.add(new GeonamesPlace(item.getString("summary"),
                            item.getString("lat"),
                            item.getString("lng")));
                }
            } else {
                searchResult.add(new GeonamesPlace("No information found at geonames"));
            }
        } else {
            Log.i("URL", "ErrorCode: " + urlConnection.getResponseCode());
            //searchResult.add("HttpUrlConnection ErrorCode: " + urlConnection.getResponseCode());
        }

    }

    // read from url connection
    private String readStream(InputStream in) throws IOException {
        StringBuilder sb =  new StringBuilder();

        BufferedReader reader = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
        String nextLine = "";
        while ((nextLine = reader.readLine()) != null) {
            sb.append(nextLine);
        }
        return sb.toString();
    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager manager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            manager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // shutdown() method will allow previously submitted tasks to execute before terminating,
        // shutdownNow() method prevents waiting tasks from starting and attempts to stop currently executing tasks
        if (executor != null) {
            executor.shutdown();
            Log.i("EXECUTOR", "ALL TASKS CANCELLED!");
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (isNetworkAvailable()) {
            if (parent != null) {
                URL openWeatherURL;
                GeonamesPlace geonamesPlace = (GeonamesPlace) parent.getAdapter().getItem(position);
                String latitude = geonamesPlace.getLatitud();
                String longitude = geonamesPlace.getLongitud();

                //api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={API key}
                try {
                    Uri.Builder builder = new Uri.Builder();
                    builder.scheme("http")
                            .authority("api.openweathermap.org")
                            .appendPath("data")
                            .appendPath("2.5")
                            .appendPath("weather")
                            .appendQueryParameter("lat", latitude)
                            .appendQueryParameter("lon", longitude)
                            .appendQueryParameter("lang", "es")
                            .appendQueryParameter("units", "metric")
                            .appendQueryParameter("appid", OPEN_WEATHER_API_KEY);

                    openWeatherURL = new URL(builder.build().toString());

                    startOpenWeatherBackgroundTask(openWeatherURL);

                } catch (MalformedURLException e) {
                    Log.i("URL", e.getMessage());
                }
            }
        } else {
            Toast.makeText(this, "Sorry, network is not available", Toast.LENGTH_LONG).show();
        }
    }

    private void startOpenWeatherBackgroundTask(URL url) {
        final int CONNECTION_TIMEOUT = 10000;
        final int READ_TIMEOUT = 7000;

        executor = Executors.newSingleThreadExecutor();

        executor.execute(new Runnable() {
            @Override
            public void run() {
                HttpURLConnection urlConnection = null;
                try {
                    // 1. Create connection object by invoking the openConnection method on a URL
                    urlConnection = (HttpURLConnection) url.openConnection();

                    // 2. Setup connection parameters
                    urlConnection.setConnectTimeout(CONNECTION_TIMEOUT);
                    urlConnection.setReadTimeout(READ_TIMEOUT);

                    // 3. connect to remote object (urlConnection)
                    urlConnection.connect();

                    // 4. The remote object becomes available.
                    //    The header fileds and the contents of the remote object can be accessed
                    getOpenWeaterData(urlConnection);

                } catch (IOException e) {
                    Log.i("IOException", e.getMessage());
                    //searchResult.add("IOException: " + e.getMessage());
                } catch (JSONException e) {
                    Log.i("JSONException", e.getMessage());
                    //searchResult.add("JSONException: " + e.getMessage());
                } finally {
                    if (urlConnection != null) urlConnection.disconnect();
                }
            }
        });

    }

    private void getOpenWeaterData(HttpURLConnection urlConnection)
            throws IOException, JSONException {

        // Check if response was OK (response=200)
        if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            // read data stream from url
            String resultStream = readStream(urlConnection.getInputStream());

            // create JSON object from result stream
            JSONObject json = new JSONObject(resultStream);
            JSONObject coord = json.getJSONObject("coord");
            JSONObject main = json.getJSONObject("main");

            JSONArray weather = json.getJSONArray("weather");
            String text = "Condiciiones climáticas en {" + coord.getString("lat").substring(0, 5) + ", "
                    + coord.getString("lon").substring(0, 5) + "}" + System.lineSeparator()
                    + "Temperatura: " + main.getString("temp") + " C" + System.lineSeparator()
                    + "Humedad: " + main.getString("humidity") + " %" + System.lineSeparator()
                    + weather.getJSONObject(0).getString("description");

            showToast(text);

        } else {
            Log.i("URL", "ErrorCode: " + urlConnection.getResponseCode());
            //searchResult.add("HttpUrlConnection ErrorCode: " + urlConnection.getResponseCode());
        }
    }

    private void showToast(final String text) {
        runOnUiThread(() -> Toast.makeText(this, text, Toast.LENGTH_SHORT).show());
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("SearchResult", listSearchResult);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        listSearchResult = (ArrayList<GeonamesPlace>) savedInstanceState.getSerializable("SearchResult");
        lvSearchResult.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, listSearchResult));
    }
}

















