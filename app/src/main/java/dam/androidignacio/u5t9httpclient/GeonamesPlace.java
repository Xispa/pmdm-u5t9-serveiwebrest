package dam.androidignacio.u5t9httpclient;

public class GeonamesPlace {

    String description;
    String latitud;
    String longitud;

    public GeonamesPlace(String description) {
        this.description = description;
    }

    public GeonamesPlace(String description, String latitud, String longitud) {
        this.description = description;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public String getLatitud() {
        return latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    @Override
    public String toString() {
        return description + '\n' + "LAT = " + latitud + ", LON = " + longitud;
    }
}
